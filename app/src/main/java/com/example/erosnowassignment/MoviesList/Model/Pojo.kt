package com.example.erosnowassignment.MoviesList.Model

class Pojo {
    var id: Int = 0
    var name: String? = null
    var description: String? = null
    var price: Double = 0.toDouble()

    constructor() : super() {}

    constructor(id: Int, name: String, description: String, price: Double) : super() {
        this.id = id
        this.name = name
        this.description = description
        this.price = price
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + id
        return result
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj)
            return true
        if (obj == null)
            return false
        if (javaClass != obj.javaClass)
            return false
        val other = obj as Pojo?
        return if (id != other!!.id) false else true
    }

    override fun toString(): String {
        return ("Product [id=" + id + ", name=" + name + ", description="
                + description + ", price=" + price + "]")
    }
}

