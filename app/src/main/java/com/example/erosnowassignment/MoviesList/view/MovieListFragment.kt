package com.example.erosnowassignment.MoviesList.view


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.example.erosnowassignment.EndlessRecyclerViewScrollListener
import com.example.erosnowassignment.MoviesList.Model.DataPojo
import com.example.erosnowassignment.MoviesList.Model.DataResultPojo
import com.example.erosnowassignment.R
import com.example.erosnowassignment.utils.RequestAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList as ArrayList1


class MovieListFragment : Fragment(){


    val dataPojo:DataPojo? = null
    var dataList: ArrayList<DataPojo>? = ArrayList()
    var recyclerView:RecyclerView? = null
    lateinit var progressBar: ProgressBar
    var movieListAdapter: MoviesListAdapter? = null
    private var pageNo: Int = 1
    private var pageCount: Int = 1
    private var oldRange: Int = 1
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    val api:String = "/3/movie/popular?api_key=fe5c9e9f1e1d5d223ebf5935b6a05735&language=en-US&page="
    private var mCompositeDisposable: CompositeDisposable? = null
    val ARG_ITEM_ID = "movie_list"

    companion object {
        fun newInstance(): MovieListFragment =
            MovieListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        if (dataList == null || dataList?.isEmpty()!! || view == null) {
            val view = inflater.inflate(R.layout.fragment_movie_list, container, false)

            recyclerView = view?.findViewById(R.id.recyler_view)
        progressBar = view?.findViewById(R.id.progressbar)!!
            val layoutManager = GridLayoutManager(activity, 2)
            recyclerView!!.layoutManager = LinearLayoutManager(activity)
            recyclerView!!.layoutManager = layoutManager


            if (pageNo <= pageCount) {
                scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
                    override fun getFooterViewType(defaultNoFooterViewType: Int): Int {
                        return -1
                    }

                    override fun onLoadMore(page: Int, totalItemsCount: Int) {
                        Log.i("page", "" + page)
                        progressBar.visibility = View.VISIBLE
                        getApi(pageNo)
                    }
                }
            }
            recyclerView?.addOnScrollListener(this.scrollListener!!)
            mCompositeDisposable = CompositeDisposable()

            pageNo = 1
            getApi(1)
             progressBar.visibility = View.VISIBLE

//           }

        return view
    }

    private fun getApi(pageNo: Int) {
        val requestAPI = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(RequestAPI::class.java)

        mCompositeDisposable!!.add(requestAPI!!.getMovieList(api + pageNo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError)
        )
    }

    private fun handleResponse(dataPojo: DataResultPojo) {
        progressBar.visibility = View.GONE

        if(dataList == null)
            dataList = ArrayList()

            pageNo++
            pageCount = dataPojo.page!!
            oldRange = dataPojo.results?.size!!

        for (j: Int in dataPojo.results!!.indices) {
            dataList?.add(dataPojo.results!![j])
        }
        if (movieListAdapter == null && recyclerView?.adapter == null) {
            movieListAdapter = MoviesListAdapter(dataList, activity)
            recyclerView!!.adapter = MoviesListAdapter(dataList, activity)

        }else{
                recyclerView?.adapter?.notifyItemRangeChanged(oldRange, dataPojo.results?.size!!)

            }




        Toast.makeText(activity, "Network Manager Works", Toast.LENGTH_LONG)

    }

    private fun handleError(error: Throwable?) {
        progressBar.visibility = View.GONE
        Toast.makeText(activity, "Issue", Toast.LENGTH_LONG)

    }
}
