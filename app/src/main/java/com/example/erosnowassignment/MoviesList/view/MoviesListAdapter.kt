package com.example.erosnowassignment.MoviesList.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.erosnowassignment.MoviesList.Model.DataPojo
import android.content.Intent
import android.databinding.DataBindingUtil
import com.example.erosnowassignment.MovieDetails.view.MovieDetailsActivity
import com.example.erosnowassignment.R
import com.example.erosnowassignment.databinding.ListItemBinding
import com.example.erosnowassignment.utils.SharedPreference


class MoviesListAdapter(var item: ArrayList<DataPojo>?, var context: Context?) : RecyclerView.Adapter<MoviesListAdapter.ViewHolder>() {
     var sharedPreference: SharedPreference? = null
     var list = item

    init {
        this.sharedPreference = SharedPreference()
    }


    override fun getItemCount(): Int {
            return item?.size!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, view: Int): ViewHolder {
        val listItemBinding = DataBindingUtil.inflate<ListItemBinding>(LayoutInflater.from(parent.context), R.layout.list_item, parent, false)
        return ViewHolder(listItemBinding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var holder = holder as ViewHolder

        holder.itemview.textName?.text = item?.get(position)?.title
        holder.itemview.overviewName.text = item?.get(position)?.overview
        holder.itemview.votes.text = item?.get(position)?.voteAverage.toString()
        holder.itemview.favouriteIcon.setImageResource(R.drawable.favourite_icon)


        var imageUrl = "https://image.tmdb.org/t/p/w400" + item?.get(position)?.posterPath
        Glide.with(this!!.context!!)
            .load(imageUrl)//TODO check image width
            .into(holder.itemview.movieImage)
        holder.itemview.favouriteIcon.setOnClickListener(View.OnClickListener {
            if (sharedPreference != null) {
                sharedPreference!!.addFavorite(context, item?.get(position))
                Toast.makeText(context, "saved to favourites", Toast.LENGTH_SHORT).show()
                holder.itemview.favouriteIcon.setImageResource(R.drawable.icon_favourite_red)
            }else{
                holder.itemview.favouriteIcon.setImageResource(R.drawable.favourite_icon)
            }

        })
        holder.itemview.parentLayout.setOnClickListener(View.OnClickListener {
            var id = item?.get(position)?.id
            val intent = Intent(context, MovieDetailsActivity::class.java)
            intent.putExtra("id", id.toString())
            context?.startActivity(intent)
        })

    }

    class ViewHolder (view: ListItemBinding) : RecyclerView.ViewHolder(view.root) {
        var itemview: ListItemBinding = view
    }


    fun add(product: DataPojo) {
        list?.add(product)
        notifyDataSetChanged()
    }

    fun remove(product: DataPojo) {
        list?.remove(product)
        notifyDataSetChanged()


    }

}


