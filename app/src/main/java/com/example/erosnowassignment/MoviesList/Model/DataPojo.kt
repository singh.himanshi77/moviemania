package com.example.erosnowassignment.MoviesList.Model

import com.example.erosnowassignment.MoviesList.viewModel.MovieListViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DataPojo {


    var voteCount: Int? = null
    var id: Int? = null
    var video: Boolean? = null
    var voteAverage: Double? = null
    var title: String? = null
    var popularity: Double? = null
    var posterPath: String? = null
    var originalLanguage: String? = null
    var originalTitle: String? = null
    var genreIds: List<Int>? = null
    var backdropPath: String? = null
    var adult: Boolean? = null
    var overview: String? = null
    var releaseDate: String? = null
}
