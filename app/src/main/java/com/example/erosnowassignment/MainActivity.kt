package com.example.erosnowassignment

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.example.erosnowassignment.FavouritesList.FavouriteListFragment
import com.example.erosnowassignment.MoviesList.view.MovieListFragment
import com.example.erosnowassignment.Search.SearchFragment

import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    var toolbar:Toolbar? = null
    var searchView:SearchView? = null
    private var query: String? = ""
    private var lastQuery: String? = null




    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                    val movieListFragment = MovieListFragment.newInstance()
                openFragment(movieListFragment)
                toolbar?.title = "Home"
                toolbar?.setTitleTextColor(resources.getColor(R.color.colorPrimaryDark))
                return@OnNavigationItemSelectedListener true

            }
            R.id.navigation_favourites -> {
                val favouriteListFragment = FavouriteListFragment()
                openFragment(favouriteListFragment)
                toolbar?.title = "Favourites"
                toolbar?.setTitleTextColor(resources.getColor(R.color.colorPrimaryDark))
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        searchView = findViewById(R.id.search_view)
                initSearch()


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val fragment = MovieListFragment()
        openFragment(fragment)

    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }
    }

    private fun initSearch() {
        search_view.setVisibility(View.VISIBLE)
        search_view.setIconified(true);
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchView?.setOnClickListener { view ->

            if (!TextUtils.isEmpty(query)) {
                lastQuery = query
            } else {
                Toast.makeText(this, "Please enter the text", Toast.LENGTH_SHORT).show()
            }
        }
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                val bundle = Bundle()
                bundle.putString("Query", query)
                val searchFragment = SearchFragment()
                searchFragment.arguments = bundle
                openFragment(searchFragment)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                query = newText

                return false
            }
        })




    }



    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}




