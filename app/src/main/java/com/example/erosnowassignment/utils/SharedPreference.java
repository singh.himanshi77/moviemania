package com.example.erosnowassignment.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.example.erosnowassignment.MoviesList.Model.DataPojo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPreference {
        public static final String PREFS_NAME = "PRODUCT_APP";
        public static final String FAVORITES = "Product_Favorite";

        public SharedPreference() {
            super();
        }

        // This four methods are used for maintaining favorites.
        public void saveFavorites(Context context, List<DataPojo> favorites) {
            SharedPreferences settings;
            SharedPreferences.Editor editor;

            settings = context.getSharedPreferences(PREFS_NAME,
                    Context.MODE_PRIVATE);
            editor = settings.edit();

            Gson gson = new Gson();
            String jsonFavorites = gson.toJson(favorites);

            editor.putString(FAVORITES, jsonFavorites);

            editor.commit();
        }

        public void addFavorite(Context context, DataPojo product) {
            List<DataPojo> favorites = getFavorites(context);
            if (favorites == null)
                favorites = new ArrayList<DataPojo>();
            favorites.add(product);
            saveFavorites(context, favorites);
        }

        public void removeFavorite(Context context, DataPojo product) {
            ArrayList<DataPojo> favorites = getFavorites(context);
            if (favorites != null) {
                favorites.remove(product);
                saveFavorites(context, favorites);
            }
        }

        public ArrayList<DataPojo> getFavorites(Context context) {
            SharedPreferences settings;
            List<DataPojo> favorites;

            settings = context.getSharedPreferences(PREFS_NAME,
                    Context.MODE_PRIVATE);

            if (settings.contains(FAVORITES)) {
                String jsonFavorites = settings.getString(FAVORITES, null);
                Gson gson = new Gson();
                DataPojo[] favoriteItems = gson.fromJson(jsonFavorites,
                        DataPojo[].class);

                favorites = Arrays.asList(favoriteItems);
                favorites = new ArrayList<DataPojo>(favorites);
            } else
                return null;

            return (ArrayList<DataPojo>) favorites;
        }
    }


