package com.example.erosnowassignment.utils

import com.example.erosnowassignment.MovieDetails.model.MovieDetailsResponsePojo
import com.example.erosnowassignment.MoviesList.Model.DataResultPojo
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface RequestAPI{
    @GET
    fun getMovieList(@Url url: String?): Observable<DataResultPojo>

    @GET
    fun getSearchResponse(@Url url: String?): Observable<DataResultPojo>

    @GET
    fun getDetailResponse(@Url url: String?): Observable<MovieDetailsResponsePojo>
}
