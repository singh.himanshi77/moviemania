package com.example.erosnowassignment.Search

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.example.erosnowassignment.EndlessRecyclerViewScrollListener
import com.example.erosnowassignment.MoviesList.Model.DataPojo
import com.example.erosnowassignment.MoviesList.Model.DataResultPojo
import com.example.erosnowassignment.MoviesList.view.MoviesListAdapter
import com.example.erosnowassignment.R
import com.example.erosnowassignment.databinding.ActivityMovieDetailsBinding
import com.example.erosnowassignment.databinding.FragmentMovieListBinding
import com.example.erosnowassignment.utils.RequestAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.ArrayList

class SearchFragment : Fragment(){
    var recyclerView:RecyclerView? = null
    var movieListAdapter: MoviesListAdapter? = null
    private var baseUrl: String? = "https://api.themoviedb.org/"
    private var api: String? = baseUrl + "3/search/movie?api_key=fe5c9e9f1e1d5d223ebf5935b6a05735&language=en-US&include_adult=false&"
    var dataList: ArrayList<DataPojo>? = ArrayList()
    lateinit var progressBar: ProgressBar
    private var mCompositeDisposable: CompositeDisposable? = null
    private var pageNo: Int = 1
    private var pageCount: Int = 1
    private var oldRange: Int = 1
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    lateinit var fragmentMovieListBinding: com.example.erosnowassignment.databinding.FragmentMovieListBinding




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        if (dataList == null || dataList?.isEmpty()!! || view == null) {
        fragmentMovieListBinding = DataBindingUtil.inflate<FragmentMovieListBinding>(LayoutInflater.from(activity), R.layout.activity_movie_details, container, false)
        val query = arguments!!.getString("Query")


        val layoutManager = GridLayoutManager(activity, 2)
        fragmentMovieListBinding.recylerView.layoutManager = LinearLayoutManager(activity)
        fragmentMovieListBinding.recylerView.layoutManager = layoutManager

        if (pageNo <= pageCount) {
            scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
                override fun getFooterViewType(defaultNoFooterViewType: Int): Int {
                    return -1
                }

                override fun onLoadMore(page: Int, totalItemsCount: Int) {
                    Log.i("page", "" + page)
                    progressBar.visibility = View.VISIBLE
                    api= api + "query=" + query + "&page=" + pageNo
                    fetchSearchResponse(api)
                }
            }
        }
        recyclerView?.addOnScrollListener(this.scrollListener!!)
        mCompositeDisposable = CompositeDisposable()
        if(api != null){
            api= api + "query=" + query + "&page=" + pageNo
            fetchSearchResponse(api)
            progressBar.visibility = View.VISIBLE

        }

        return fragmentMovieListBinding.root
    }
    private fun fetchSearchResponse(apiUrl: String?) {

        val requestInterface = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(RequestAPI::class.java)

        mCompositeDisposable?.add(requestInterface!!.getSearchResponse(api)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse, this::handleError))
    }

    private fun handleResponse(dataPojo: DataResultPojo) {
        progressBar.visibility = View.GONE

        if(dataList == null)
            dataList = ArrayList()

        pageNo++
        oldRange = dataPojo.results?.size!!

        for (j: Int in dataPojo.results!!.indices) {
            dataList?.add(dataPojo.results!![j])
        }

        if (movieListAdapter == null && recyclerView?.adapter == null) {
            movieListAdapter = MoviesListAdapter(dataList, activity)
            recyclerView!!.adapter = MoviesListAdapter(dataList, activity)

        }else{
            recyclerView?.adapter?.notifyItemRangeChanged(oldRange, dataPojo.results?.size!!)

        }
        Toast.makeText(activity, "Network Manager Works", Toast.LENGTH_LONG).show()

    }

    private fun handleError(error: Throwable?) {
        progressBar.visibility = View.GONE
        Toast.makeText(activity, "Issue", Toast.LENGTH_LONG).show()

    }


}
