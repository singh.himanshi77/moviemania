package com.example.erosnowassignment.FavouritesList

import android.app.Activity
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.erosnowassignment.MoviesList.Model.DataPojo
import com.example.erosnowassignment.R
import com.example.erosnowassignment.databinding.FavouriteFragmentListBinding
import com.example.erosnowassignment.databinding.FragmentMovieListBinding
import com.example.erosnowassignment.utils.SharedPreference

import java.util.ArrayList

class FavouriteListFragment : Fragment() {

    internal var sharedPreference: SharedPreference? = null
    internal lateinit var recyclerView: RecyclerView
    var favorites: ArrayList<DataPojo>? = null
    lateinit var progressBar: ProgressBar
    lateinit var favouriteFragmentListBinding: FavouriteFragmentListBinding



    internal var activity: Activity? = null
    var moviesListAdapter: FavouritesAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        favouriteFragmentListBinding = DataBindingUtil.inflate<FavouriteFragmentListBinding>(LayoutInflater.from(activity), R.layout.activity_movie_details, container, false)

        // Get favorite items from SharedPreferences.
        progressBar = view?.findViewById(R.id.progressbar)!!

        sharedPreference = SharedPreference()
        favorites = sharedPreference?.getFavorites(activity)
        val layoutManager = GridLayoutManager(activity, 2)
        favouriteFragmentListBinding.recylerView.layoutManager = LinearLayoutManager(activity)
        favouriteFragmentListBinding.recylerView.layoutManager = layoutManager



            if (favorites != null) {
                for(j: Int in favorites!!.indices) {
                    moviesListAdapter =
                        FavouritesAdapter(favorites, activity)
                    favouriteFragmentListBinding.recylerView.adapter = moviesListAdapter
                }

            }

        return favouriteFragmentListBinding.root
    }

    companion object {
        val ARG_ITEM_ID = "favorite_list"
    }

}
