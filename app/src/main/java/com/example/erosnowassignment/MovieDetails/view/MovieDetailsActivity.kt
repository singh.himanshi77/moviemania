package com.example.erosnowassignment.MovieDetails.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import android.widget.*
import com.example.erosnowassignment.MovieDetails.model.MovieDetailsResponsePojo
import com.example.erosnowassignment.R
import com.example.erosnowassignment.databinding.ActivityMovieDetailsBinding
import com.example.erosnowassignment.utils.RequestAPI
import kotlinx.android.synthetic.main.activity_main.*


class MovieDetailsActivity : AppCompatActivity(){
    private var mCompositeDisposable: CompositeDisposable? = null
    var paramString = "?api_key=fe5c9e9f1e1d5d223ebf5935b6a05735&language=en-US"
    var preString = "3/movie/"

    private var baseurl = "https://api.themoviedb.org/"
    lateinit var activityMovieDetailsBinding: ActivityMovieDetailsBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMovieDetailsBinding = DataBindingUtil.inflate<ActivityMovieDetailsBinding>(LayoutInflater.from(this), R.layout.activity_movie_details, container, false)
        val i = intent
        val movieId = i.getStringExtra("id")

        mCompositeDisposable = CompositeDisposable()

        var apiUrl = preString + movieId + paramString
        getApi(apiUrl)
        activityMovieDetailsBinding.progressbar.visibility = View.VISIBLE

    }

    private fun getApi(api:String) {
        val requestAPI = Retrofit.Builder()
            .baseUrl(baseurl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(RequestAPI::class.java)

        mCompositeDisposable!!.add(requestAPI!!.getDetailResponse(api)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse, this::handleError)
        )
    }

    private fun handleResponse(dataPojo: MovieDetailsResponsePojo) {
        activityMovieDetailsBinding.progressbar.visibility = View.GONE
        if(dataPojo != null){
            var imageUrl = "https://image.tmdb.org/t/p/w500" + dataPojo.poster_path
            Glide.with(this).load(imageUrl).into(activityMovieDetailsBinding.movieImage)

            activityMovieDetailsBinding.textName.text = dataPojo.original_title
            activityMovieDetailsBinding.releaseDate.text = dataPojo.release_date
            activityMovieDetailsBinding.votes.text = dataPojo.vote_average.toString()
            activityMovieDetailsBinding.overviewName.text = dataPojo.overview
            activityMovieDetailsBinding.genre.text = dataPojo.genres?.get(0)?.name

        }

    }

    private fun handleError(error: Throwable?) {
        activityMovieDetailsBinding.progressbar.visibility = View.GONE
        Toast.makeText(this, error?.localizedMessage, Toast.LENGTH_SHORT).show()

    }


}
