package com.example.erosnowassignment.MovieDetails.model


class MovieDetailsResponsePojo {

    var adult: Boolean? = null
    var backdrop_path: String? = null
    var belongsToCollection: Any? = null
    var budget: Int? = null
    var genres: List<Genre>? = null
    var homepage: String? = null
    var id: Int? = null
    var imdbId: String? = null
    var originalLanguage: String? = null
    var original_title: String? = null
    var overview: String? = null
    var popularity: Double? = null
    var poster_path: String? = null
    var release_date: String? = null
    var revenue: Double? = null
    var runtime: Int? = null
    var status: String? = null
    var tagline: String? = null
    var title: String? = null
    var video: Boolean? = null
    var vote_average: Double? = null
    var voteCount: Int? = null

}


